package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected Map<String, E> map = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public void add(@NotNull final E entity) {
        map.put(entity.getId(), entity);
    }

    @NotNull
    @Override
    public Optional<E> findById(@NotNull final String id) {
        return Optional.ofNullable(map.get(id));
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public void remove(@NotNull final E entity) {
        map.remove(entity.getId());
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
