package ru.arubtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Lock User:");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("User " + login + " was locked");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
