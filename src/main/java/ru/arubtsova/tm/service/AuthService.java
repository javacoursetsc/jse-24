package ru.arubtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.IAuthService;
import ru.arubtsova.tm.api.service.IUserService;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.exception.authorization.AccessDeniedException;
import ru.arubtsova.tm.exception.empty.EmptyLoginException;
import ru.arubtsova.tm.exception.empty.EmptyPasswordException;
import ru.arubtsova.tm.exception.entity.UserNotFoundException;
import ru.arubtsova.tm.model.User;
import ru.arubtsova.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public Optional<User> getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRole(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final Optional<User> user = getUser();
        if (!user.isPresent()) throw new AccessDeniedException();
        @NotNull final Role role = user.get().getRole();
        if (role == null) throw new AccessDeniedException();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final Optional<User> user = userService.findByLogin(login);
        user.orElseThrow(UserNotFoundException::new);
        if (user.get().isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    public void register(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        userService.create(login, password, email);
    }

}
